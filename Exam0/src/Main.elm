module Main exposing (DiGraph, dfs,  outNeigh, vertices, getLength, innerLength, checkInnerLength, getDiagonal, diagonal, getAtIndex, getAtRow)

------------------ Ex. 1 ---------------------
{-
    Write a function
      diagonal : List (List Int) -> Maybe (List Int)
   That accepts a square matrix (as a list of lists) and returns its diagonal.
   If the input is not a valid square matrix it should return Nothing.

   Example:
   diagonal [[1,2,3],[1,4,3],[1,2,5]] returns Just [1,4,5]
   diagonal [[1,2],[1,2,3]] returns Nothing

    Write a function
      transpose : List (List Int) -> List (List Int)
     which, given a matrix (not neccessarily a square one), returns its transposition.

    Example:
    transpose [[1,2],[3,4],[5,6]]  returns [[1,3,5],[2,4,6]]

   For the implementation you can use only the List module in the standard API.

-}

diagonal : List (List Int) -> Maybe (List Int)
diagonal m =
    if checkInnerLength (innerLength m) (getLength m) == False then
        Nothing
    else 
        Just (getDiagonal m 0 0 (getLength m))


getDiagonal : List (List Int) -> Int -> Int -> Int -> (List Int)
getDiagonal m first second limit = 
    if first == limit then 
        []
    else 
        (getAtIndex (getAtRow m first) second) :: (getDiagonal m (first+1) (second+1) limit)

getAtIndex: List Int -> Int -> Int
getAtIndex l index =
    if index == 0 then 
        Maybe.withDefault 0 (List.head l) 
    else 
        getAtIndex (Maybe.withDefault [] (List.tail l)) (index-1)

getAtRow: List(List Int) -> Int -> List Int
getAtRow l index = 
    if index == 0 then 
        Maybe.withDefault [] (List.head l) 
    else 
        getAtRow (Maybe.withDefault [] (List.tail l)) (index-1)

getLength : List (List Int) -> Int 
getLength m = 
    List.length m

innerLength : List (List Int) -> List Int
innerLength m =  
    case m of 
        [] -> []

        _ -> 
            List.length (Maybe.withDefault [] (List.head m)) :: innerLength (Maybe.withDefault [] (List.tail m))    
    
checkInnerLength : List Int -> Int -> Bool
checkInnerLength l n =
    if (Maybe.withDefault 0 (List.head l)) == n then 
        checkInnerLength (Maybe.withDefault [] (List.tail l)) n 

    else if (Maybe.withDefault 0 (List.head l)) == 0 then 
        True 

    else
        False 



transpose : List (List Int) -> List (List Int)
transpose m =
    []



------------------ Ex. 2 ---------------------
{-
   A directed graph can be represented as a list of its edges:
   type alias DiGraph =
       List ( Int, Int )
   Implement:
           a) the function
               vertices : DiGraph -> List Int
              which extracts a list of all the vertices (without duplication) from the given graph.
           b) the function
               outNeigh : DiGraph -> Int -> List Int
              which, given a graph and a vertex, returns a list of all its out-neighbors
           c) the function
               dfs : DiGraph -> List Int
               which, given a graph, makes a dfs traversal of the graph (you can start in any vertex).


   For the implementation you can use only the List module in the standard API.
-}


type alias DiGraph =
    List ( Int, Int )


vertices : DiGraph -> List Int
vertices g =
    []


outNeigh : DiGraph -> Int -> List Int
outNeigh g v =
    []


dfs : DiGraph -> List Int
dfs g =
    []