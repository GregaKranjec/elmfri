module Main exposing (BigNum, add,  fromString, toString)

------------------ Ex. 1 ---------------------
{-
   In the first exercise you will be working with arbitrary large integers.

   Let arbitrary integers be represented as a list of numerals in the decimal system, e.g. the number 321 is represented as the list [1,2,3] (notice the numerals are in the reverse order from the classical representation - this is to make it easier for you).


   First implement two functions for dealing with the representation
   fromString : String -> Maybe BigNum
   toString : BigNum -> String
   The first function returns a BigNum representation, give a String, e.g. "321" -> Just [1,2,3].
   If the string is not a valid representation, it should return Nothing.
   The second function returns a BigNum back into the String representation, e.g. [1,2,3] -> "321".


   Your second goal is to implement addition of numbers in this representation.
   add: BigNum -> BigNum -> BigNum

     For the implementation you can use only the List, String, and Char module in the standard API.
-}


type alias BigNum =
    List Int

fromString : String -> Maybe BigNum
fromString s =
    Just (List.map(\ x->x-48) (List.reverse(List.map Char.toCode (String.toList s))))


toString : BigNum -> String
toString b =
    if b == [] then 
        ""
    else 
        let 
            head = List.head b 
            tail = List.tail b
        in
            toString (Maybe.withDefault [] tail) ++ String.fromInt (Maybe.withDefault -1 head)


add : BigNum -> BigNum -> Int -> BigNum
add n1 n2 resd =
    if n1 == [] && n2 == [] && resd == 0  then 
        [] 
    else 
        let 
            sum = Maybe.withDefault 0 (List.head n1) + Maybe.withDefault 0 (List.head n2)
            t1 = List.tail n1 
            t2 = List.tail n2 
        in 
            if (sum+resd) < 10 then
                (sum+resd) :: add (Maybe.withDefault [] t1) (Maybe.withDefault [] t2) 0
            else 
                (modBy 10 (sum+resd)) :: add (Maybe.withDefault [] t1) (Maybe.withDefault [] t2) ((sum+resd)//10)



------------------ Ex. 2 ---------------------
{-
   Your task is to provide a truth table for a boolean function.
   The boolean functions will be simply denoted as
   type alias BoolFunction =
       List Bool -> Bool

   your task is to create a truth table for any given BoolFunction.

   You will do this in two steps:

   1. First write a function:
   allVarValues : Int -> List (List Bool)
   which takes as input an integer (i.e. the number of boolean variables). The return value is a list of list of booleans. Each list represents a set of values for the boolean function.

   2. Then write the function that constructs the truth table
   genTruth : BoolFunction -> Int -> TruthTable


   Example:
   allVarValues 2 = [[True,True],[False,True],[True,False],[False,False]]

   and e.g. we have a function (basically just (A or B))
   someF x =
       case x of
           [ a, b ] ->
               a || b

           _ ->
               False

   The truth table for this function (we provide the knowledge that it has only two variables)
   genTruth someF 2 = [([True,True],True),([False,True],True),([True,False],True),([False,False],False)]
     For the implementation you can use only the List module in the standard API.
-}


type alias BoolFunction =
    List Bool -> Bool


type alias TruthTable =
    List ( List Bool, Bool )


allVarValues : Int -> List (List Bool)
allVarValues n =
    []


genTruth : BoolFunction -> Int -> TruthTable
genTruth f n =
    []