module Main exposing (BigNum, Expr(..), add, simplify, toString)

------------------ Ex. 1 ---------------------
{-
   In the first exercise you will be working with arbitrary large integers.

   Let arbitrary integers be represented as a list of numerals in the decimal system, e.g. the number 321 is 
   represented as the list [1,2,3] (notice the numerals are in the reverse order from the classical representation - this is to make it easier for you).


   First implement two functions for dealing with the representation
   fromString : String -> Maybe BigNum
   toString : BigNum -> String
   The first function returns a BigNum representation, give a String, e.g. "321" -> Just [1,2,3].
   If the string is not a valid representation, it should return Nothing.
   The second function returns a BigNum back into the String representation, e.g. [1,2,3] -> "321".


   Your second goal is to implement addition of numbers in this representation.
   add: BigNum -> BigNum -> BigNum

     For the implementation you can use only the List, String, and Char module in the standard API.
-}


type alias BigNum =
    List Int

{-
fromString : String -> Maybe BigNum
fromString s =
    fromString2 s (String.length s)


fromString2 : String -> Int -> Maybe BigNum
fromString2 s pos = 
    if pos > 0 then
       List.singleton (String.toInt(String.slice pos pos-1)) :: fromString2 s (pos-1)
    else
        Nothing
-}

toString : BigNum -> String
toString b =
    ""


add : BigNum -> BigNum -> BigNum
add n1 n2 =
    []



------------------ Ex. 2 ---------------------
{-
    You are given a simple data structure that hold arithmetic expressions. These expressions include variables, integers, and operations + an *.

    E.g. the expression x+3*z is represented as:
        Plus (Var "x") (Times (Val 3) (Var "z"))

    You have to implement the function:
        simplify : Expr -> Expr
   which simplifies all the constant subexpressions, by computing their value.
   E.g. the expression 3+5*7 is constant. The function should therefore return (Val 38).
   If the given expr. would be 3*x+y+(3+3+3)  then the simplification would return 3*x+y+9.
-}


type Expr
    = Var String
    | Val Int
    | Plus Expr Expr
    | Times Expr Expr


simplify : Expr -> Expr
simplify expr =
    case expr of 
        Var s -> 
            Var s
        
        Val i -> 
            Val i
        
        Plus e1 e2 ->
            if (checkIfVal e1) == True && (checkIfVal e2) == True then 
                



checkIfVal : Expr -> Bool
checkIfVal expr =
    case expr of 
        Val i ->
            True 
        _ ->
            False

        

