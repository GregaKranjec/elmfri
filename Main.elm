module Main exposing (..)
import Browser exposing (sandbox)
import Html exposing(..)
import Html.Events exposing (onClick)
import Html.Attributes exposing(..)

type alias LightsLine = List Bool
type Msg = Toggle Int

toggleElement : Int -> List Bool -> List Bool
toggleElement idx l =
    case ( l, idx ) of
        ( [], _ ) ->
            l

        ( e :: t, 0 ) ->
            not e :: t

        ( e :: t, i ) ->
            e :: toggleElement (idx - 1) t

view : LightsLine -> Html Msg
view l=
     let
        newL = List.indexedMap Tuple.pair l
     in
     div[]
           (List.map(\x-> if Tuple.second(x) == True then button[onClick (Toggle (Tuple.first(x))) ][img[src "https://bytebucket.org/elmfri/week6/raw/df08d0843e52f77913103cd6e3bcbd87657cea14/src/bulb-on.png"][]] else button[onClick (Toggle (Tuple.first(x)))] [img[src "https://bytebucket.org/elmfri/week6/raw/df08d0843e52f77913103cd6e3bcbd87657cea14/src/bulb-off.png"][]] ) newL)

init: Int -> LightsLine
init i =
       List.map (\x -> if modBy 2 x ==0 then True else False) (List.range 0 i)
update : Msg -> LightsLine -> LightsLine
update msg l =
       let
         newL = List.indexedMap Tuple.pair l
       in
       case msg of
            Toggle i -> List.map(\x -> if Tuple.first(x) == i then not (Tuple.second(x)) else Tuple.second(x)) newL

main =
     Browser.sandbox{init= (init 6) ,update = update,view = view} 
