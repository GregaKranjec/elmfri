module Main exposing (main)

import Browser
import Html exposing (..)
import Html.Events exposing (onClick)
import Http
import Json.Decode as D


chuckurl =
    "https://api.chucknorris.io/jokes/random"


type alias Model =
    { joke : String
    }


type Msg
    = Kick
    | NewJoke (Result Http.Error String)


init : () -> ( Model, Cmd Msg )
init _ =
    ( Model "No joke yet", Cmd.none )


view : Model -> Html Msg
view m =
    Html.div [] [ Html.button [ onClick Kick ] [ Html.text "New joke" ], Html.br [] [], Html.text m.joke ]


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Kick ->
            ( model, Http.get { expect = Http.expectJson NewJoke (D.field "value" D.string), url = chuckurl } )

        NewJoke (Ok joke) ->
            ( Model joke, Cmd.none )

        NewJoke (Err _) ->
            ( Model "There was an error", Cmd.none )


main =
    Browser.element { init = init, update = update, view = view, subscriptions = \_ -> Sub.none }
