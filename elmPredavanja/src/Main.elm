module Main exposing(line, pyramid, compose)

import Html
-- # Lecture 2 - functions
{- SUMMARY:
   -functions types,
   -currying
   -functions as data,
   -anonymous functions,
   -function composition and pipes
-}

{-
f : Int -> Int
f n =
   2 * n
-}

{-
g : Int -> (Int -> Int)
g n m =
   n + 2 * m
-}

rangeMap : Int -> Int -> (Int -> String) -> String
rangeMap start end m =
      if start > end then
         ""
      else
         m start ++ rangeMap (start+1) end m


line : Int -> String
line n =
    rangeMap 1 n (\x -> "*") ++ "\n" -- (\x -> "*") prints "*" x times


pyramid : Int -> String
pyramid n =
   rangeMap 1 n (\x -> rangeMap 1 x (\y -> "*")++"\n")

compose : (Int -> Int) -> (Int -> Int) -> (Int -> Int)
compose f g =
   \x -> g(f x)


fib n =
   let
      fibfast f1 f2 m =
         if m == 1 then
            f1
         else
            fibfast f2 (f1+f2) (m-1)
   in
   fibfast 1 1 n

fibRange start end =
   if start == end then
      (String.fromInt(fib end)) ++ " "
   else
      (String.fromInt(fib start)) ++ " " ++ fibRange (start+1) end
