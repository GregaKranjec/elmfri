module Main exposing (..)

import Html exposing (..)


------------------ Naloga 1 ---------------------
{-
    Napišite funkcijo
      filterFirstK : (a -> Bool) -> Int -> List a -> List a
   ki iz podanega seznama izlušči prvih k (drugi argument) elementov,
   ki ustrezajo podanemu predikatu (prvi argument).
   Funkcijo implementirajte samo z uporabo fold(l ali r) (ostale funkcije iz List niso dovoljene). Elementi vrnjenega seznama morajo biti v istem vrstnem redu kot v podanem seznamu.
-}


filterFirstK : (a -> Bool) -> Int -> List a -> List a
filterFirstK predicate k l =
    let
        ( _, result ) =
            List.foldl
                (\a ( r, lst ) ->
                    if (r <= 0) || (not (predicate a)) then
                        ( r, lst )
                    else
                        ( r - 1, lst ++ [ a ] )
                )
                ( k, [] )
                l
    in
        result



------------------ Naloga 2 ---------------------
{-
   Napišite funkcijo tryAll, ki dobi podan predikat, ki sprejme seznam Bool vrednosti,
   ter jo preizkusi na vseh možnih vhodih dolžine length (>=0). Vrne vrednosti tega predikata za vse možne sezname te dolžine.

   Namig - pomagajte si tako, da najprej napišete funkcijo, ki vrne seznam vseh možnih seznamov.
   Pomislite kako lahko definirate ta problem rekurzivno - torej če že imate vse možne sezname dolžine (n-1),
   kako lahko iz njih zgradite vse možne sezname dolžine n.

-}


genAll : Int -> List (List Bool)
genAll n =
    if n <= 0 then
        [ [ ] ]
    else 
        List.concatMap (\l -> [ False :: l, True :: l]) (genAll (n - 1))

tryAll : (List Bool -> Bool) -> Int -> List Bool
tryAll f length =
    List.map f (genAll length)



------------------ Naloga 3 ---------------------
{-
   Podan imate tip Nat, ki predstavlja nenegativno celo število, ter tip ListWithLen,
   ki predstavlja seznam, ki s seboj nosi še dolžino (v obliki vrednosti tipa Nat).

   Za ta seznam implementirajte funkcije:
   len : ListWithLen a -> Nat
   Vrne dolžino seznama (kot Nat)

   add : a -> ListWithLen a -> ListWithLen a
   Dodajanje elementa na začetek seznama

   addEnd : a -> ListWithLen a -> ListWithLen a
   Dodajanje elementa na konec seznama

   foldl : (a -> b -> b) -> b -> ListWithLen a -> b
   Fold z leve (klasičen).

   filter : (a -> Bool) -> ListWithLen a -> ListWithLen a
   Filtriranje, za to funkcijo obvezno uporabite maloprej definiran fold.
-}


type Nat
    = Z
    | S Nat


type ListWithLen a
    = Empty
    | Cons Nat a (ListWithLen a)


len : ListWithLen a -> Nat
len l =
    case l of
        Empty ->
            Z 
        
        Cons nat a aListWithLen ->
            nat

        


add : a -> ListWithLen a -> ListWithLen a
add a l =
    case l of 
        Empty -> 
            Cons (S Z) a Empty
        
        Cons nat _ _ -> 
            Cons (S nat) a l


addEnd : a -> ListWithLen a -> ListWithLen a
addEnd a l =
    case l of 
        Empty -> 
            Cons (S Z) a Empty
        
        Cons n e rest -> 
            Cons (S n) e (addEnd a rest)


foldl : (a -> b -> b) -> b -> ListWithLen a -> b
foldl f b l =
    case l of 
        Empty -> 
            b 
        
        Cons n a rest -> 
            foldl f (f a b) rest

filter : (a -> Bool) -> ListWithLen a -> ListWithLen a
filter pred l =
    Empty


main =
    text "Izpit - Elm"