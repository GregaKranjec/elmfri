# First week exercises Elm@FRI

Clone the following repository to your computer:

```
git clone https://bitbucket.org/elmfri/week1
```
In the directory `src` the file `Main.elm`, contains the template for your solution.

In this directory  you can type `elm reactor`, you can check your functions in the browser, as shown on the lectures.


## Exercise 1
write a function

```elm
raindrops: Int -> String
```
thet accepts an integer and outputs a string:

- Pling - if n is divisible by 3
- Plang - if n is divisible by 5
- Plong - if n is divisible by 7

If n is divisible by two or even all three numbers (3,5, and 7), it should return two or all three "sounds". Eg. for the number 15 you should output "PlingPlang". Write a function that is as short as possible.


## Exercise 2
Write a function

```elm
countOnes : Int -> Int
```
which counts the number of ones in the binary representation of the input number. Eg. `countOnes 5` returns 2, `countOnes 13` returns 3.


## Exercise 3

Write a function
```elm
chessboard: Int -> String
```
that returns a chessboard with n x n fields.
Eg. `chessboard 8`  returns the string:
```
* * * *
 * * * *
* * * *
 * * * *
* * * *
 * * * *
* * * *
 * * * *   
```
## Exercise 4
Write a function

```elm
pyramid : Int -> String

```
that returns a pyramid with n lines.
Eg. `pyramid 5` returns the string:
```
    *
   ***
  *****
 *******
**********
```
