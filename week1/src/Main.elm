

module Main exposing (chessboard)

import Html



-- raindrops n =
-- countOnes n =
-- chessboard n =
-- pyramid n =

{-raindrops n =
   if modBy 3 n == 0 then
      "Pling" ++ raindropsPlang n
   else
      raindropsPlang n
raindropsPlang n =
   if modBy 5 n == 0 then
      "Plang" ++ raindropsPlong n
   else
      raindropsPlong n
raindropsPlong n =
   if modBy 7 n == 0 then
      "Plong"
   else
      " "
-}

raindrops n =
   let
      divK k m sound =
         if modBy k m == 0 then
            sound
         else
            ""
   in
      (divK 3 n "Pling")++(divK 5 n "Plong")++(divK 7 n "Plang")


--COUNT BINARY

countOnes n =
   countOnes2 n 0

countOnes2 n m =
   if n == 0 then
      m
   else
      if modBy 2 n == 0 then
         countOnes2 (n//2) m
      elseli
         countOnes2 (n//2) (m+1)


-- CHESSBOARD
chessboard q =
   let
      mainChess m n =
         if m == n then
            ""
         else
            tipVrstice m n ++ mainChess(m+1) n

      tipVrstice m n =
         if modBy 2 m == 0 then
            sodaVrstica 0 n
         else
            lihaVrstica 0 n

      sodaVrstica m n =
         if modBy 2 m == 0 then
            if m < n then
               " " ++ sodaVrstica (m+1) n
            else
               " \n"
         else
            if m < n then
               "*" ++ sodaVrstica (m+1) n
            else
               "*\n"

      lihaVrstica m n =
         if modBy 2 m == 0 then
            if m < n-1 then
               "*" ++ lihaVrstica (m+1) n
            else
               "*\n"
         else
            if m < n-1 then
               " " ++ lihaVrstica (m+1) n
            else
               " \n"

   in
   mainChess 0 q



--PYRAMID
pyramid q =
   let
      presledki n m =
         if n == 0 then
            zvezdice m
         else
            " " ++ presledki (n-1) m

      zvezdice m =
         if m == 0 then
            "\n"
         else
            "*" ++ zvezdice (m-1)

      vrstica pos end =
         if pos == end then
            ""
         else
            presledki (end-pos) (1+pos*2) ++ vrstica(pos+1) end

   in

   vrstica 0 q








main =
    --   Html.pre [] [ Html.text (raindrops 10) ]
    Html.pre [] [ Html.text (chessboard 8) ]
