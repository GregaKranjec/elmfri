# Second week Elm@FRI

In the second week we will start working more intensively with functions as "first-class citizens".  The first two parts of the assignment are obligatory, but the third part is optional.


## Sets

In the first part of you have to implement a few functions, that will work as a data structure Set (which holds only integers).
```elm
type alias Set =
    Int -> Bool
```
We defined a type of a set, which is basically a function. This function works as a set because for any given integer it reports if it the integer is contained in the set or not.

First implement three basic operations for building elementary sets.
```elm
empty : Set
singleton : Int -> Set
interval : Int -> Int -> Set
```
The first function simply returns an empty set (what is that?).
The second function return a set containing only one element (the one given as the parameter). And the third functions return a set containing the entire interval of integers (including the borders).

Then implement the union and the intersection of two sets:
```elm
union : Set -> Set -> Set
intersect : Set -> Set -> Set
```

Finally, implement the function:
```elm
show : Int -> Int -> Set -> String
```
which accepts two numbers (which represent an interval) and a set.
It should return a string representation of the given set. I.e. all the elements of the set that are in the given interval.

The return value of the function should be something like this:
```
{3, 5, 7, 8, 9}
```


## Functions of real variables
First let us define the type of such variable:

```elm
type alias RealFunc =
    Float -> Float
```
Then we implement 4 functions, which transform the input functions into new functions:

```elm
sumf : RealFunc -> RealFunc -> RealFunc
multf : RealFunc -> RealFunc -> RealFunc
compose : RealFunc -> RealFunc -> RealFunc
derive : RealFunc -> RealFunc
```
The first function returns the sum of two functions, the second function returns the product of two functions, and the third returns the composition of two functions.
The fourth function is a little bit different, it takes only one function and returns its derivative. Implement the derivation simply numerically.

Finally, in order to be able to check your work, implement a function for constructing tables for functions.

```elm
makeTable : RealFunc -> Float -> Float -> Float -> String
```
This funtion accepts 1) a function, 2) the start of the interval, 3) end of the interval, and 4) step to be  used in the interval. It returns the table as a string.

Example:
```elm
f: RealFunc
f x = x*x+3
makeTable f 0.0 1.0 0.1
```
The return value of `makeTable` should be a string, e.g. something like this:
```
   x    |       f(x)
---------------------------
0       |      3.0
0.1     |      3.01
0.2     |      3.04
0.3     |      3.09
0.4     |      3.16
0.5     |      3.25
0.6     |      3.36
0.7     |      3.49
0.8     |      3.64
0.9     |      3.91
1.0     |      4.0
```

## Encoding strings :crown:
Take the exercise below as a challenge (this is an optional exercise). Assume that the character are only ASCII, i.e. values between 0 and 255.

Any function `Char -> Char` can be seen as an encoder.

```elm
type alias Coder =
    Char -> Char
```
Write a functions that work with such encoding functions.
First write a function that takes a string and an encoder and returns the encoded string.

 ```elm
encode : String -> Coder -> String
 ```
If we want an encoding function to be useful, it has to be bijective, otherwise it is not possible to decode the string.
Write a function that checks if the given encoder is bijective.

```elm
isBijective : Coder -> Bool
```

Write a function that returns the inverse of a function.
```elm
inverse: Coder -> Coder
```
