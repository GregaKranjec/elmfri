module Main exposing (Coder, RealFunc, Set, main, singleton, interval, union, intersect, toString, show, sumf, multf, compose, derive, makeTable)

import Html exposing (..)


type alias Set =
    Int -> Bool


empty : Set
empty =
   \x -> False



singleton : Int -> Set
singleton n =
   \x -> if x == n then
            True
         else
            False

interval  : Int -> Int -> Set
interval start end =
   \x -> if x >= start && x <= end then
            True
         else
            False

union : Set -> Set -> Set
union set1 set2 =
   \x -> if set1 x || set2 x then
            True
         else
            False

intersect : Set -> Set -> Set
intersect set1 set2 =
   \x -> if set1 x && set2 x then
            True
         else
            False

toString : Int -> String
toString n =
   String.fromInt n


show : Int -> Int -> Set -> String
show start end set =
   if start == end then
      if set start then
         toString start
      else
         ""
   else
      if set start then
         toString start ++ " " ++ show (start+1) end set
      else
         show (start+1) end set




type alias RealFunc =
    Float -> Float

sumf : RealFunc -> RealFunc -> RealFunc
sumf fun1 fun2 =
   \x -> fun1 x + fun2 x

multf : RealFunc -> RealFunc -> RealFunc
multf fun1 fun2 =
   \x -> fun1 x * fun2 x

compose : RealFunc -> RealFunc -> RealFunc
compose fun1 fun2 =
   \x ->fun2 x |> fun1

derive : RealFunc -> RealFunc
derive fun =
   \x -> ((fun (x+0.00001)) - fun(x))/(0.00001)

makeTable : RealFunc -> Float -> Float -> Float -> String
makeTable fun start end step =
   if start >= end then
      String.fromFloat end ++ " | " ++ String.fromFloat (fun start)
   else
      String.fromFloat start ++ " | " ++ String.fromFloat (fun start) ++ "\n" ++ makeTable fun (start+step) end step





type alias Coder =
    Char -> Char

funkcija : Float -> Float
funkcija n = n * n

-- encode: String -> Coder -> String
-- isBijective : Coder -> Boolean


main =
    Html.pre [] [ Html.text (makeTable funkcija 1 2 0.1) ]
