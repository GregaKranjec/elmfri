# Third week of Elm@FRI

This assignment is an overview of lists and the Maybe type.

## Basic work with lists
Implement the basic operations on lists. For this implementation you can use only  the functions `head` and `tail` from the module `List`.

```elm
get: Int -> List Int -> Maybe Int
find: Int -> List Int -> Maybe Int
delete: Int -> List Int -> List Int
concat: List Int -> List Int -> List Int
map: List Int -> (Int->Int) -> List Int
foldLeft: Int -> List Int -> (Int->Int->Int) -> Int
```
- Function 'get'  receives an index (integer) and a list. It should return the element on that index  or 'Nothing', if the index is not valid.
- Function 'find' searches for the given element and returns the index of its first occurrence, or 'Nothing', if the element is not in the list.
- Function 'delete' removes all occurrences of the given element. If the element is not in the list, then the list remains the same.
- Function 'concat' takes two lists and returns the concatenation of the two.
- Function 'map' takes a list and a function. It should return a list with the same size, where each element was mapped with the given function
- Function 'foldLeft' takes the initial element (state), a list, and a function, which merges one element of the list with the current state. It returns the result of the final merging.



## Encoding with lists
Write a function, which will encode a string with LU/MTF encoding.

1. LU/MTF (List Update/ Move To Front) encoding transform each character in the string with its position in a certain linked list.
2. This list is not static, after each char that has been encoded, it changes.
3. We change the list by moving the element in front of the list.
4. Let us assume the string will be sequences of ASCII characters, that is why we can start with a list of values from 0 to 255.

```elm
startList: List Int
```
This is the initial list, holding values from 0 to 255.


```elm
findAndMTF: Char -> List Int -> Maybe (Int, List Int)
```
This function accepts a single character and the encoding list. It return the position of the given character and the corrected list.



```elm
encodeMTF: String -> Maybe (List Int)
```
With the previously defined function you can now write the encoding function.
