module Main exposing (main)
import Char
import String
import Html exposing (..)



---------------------------------------------------------------


get: Int -> List Int -> Maybe Int
get n l =
   if (l == []) then
      Nothing
   else if (n == 0) then
      List.head(l)
   else
      get (n-1) (Maybe.withDefault [] (List.tail l))

find: Int -> List Int -> Maybe Int
find n l =
   if (l == []) then
      Nothing
   else if (n == Maybe.withDefault 0 (List.head l)) then
      Just 0
   else
      Just (1 + Maybe.withDefault 0 (find n (Maybe.withDefault [] (List.tail l))))

delete: Int -> List Int -> List Int
delete n l =
   filter l (\x -> x /= n)

filter : List Int -> (Int -> Bool) -> List Int
filter l f =
   let
      h = List.head l
      t = List.tail l
   in
      if h == Nothing then
         []
      else if f (Maybe.withDefault 0 h) then
         (Maybe.withDefault 0 h) :: (filter (Maybe.withDefault [] t) f)
      else
         filter (Maybe.withDefault [] t) f

concat: List Int -> List Int -> List Int
concat l1 l2 =
   if l1 == [] then
      l2
   else
      Maybe.withDefault 0 (List.head l1) :: concat (Maybe.withDefault [] (List.tail l1)) l2


map: List Int -> (Int->Int) -> List Int
map l f =
   if l == [] then
      []
   else
      f (Maybe.withDefault 0 (List.head l)) :: (map (Maybe.withDefault [] (List.tail l)) f)

foldLeft: Int -> List Int -> (Int->Int->Int) -> Int
foldLeft state l f =
   if l == [] then
      0
   else
      f (foldLeft (f state (Maybe.withDefault 0 (List.head l))) (Maybe.withDefault [] (List.tail l)) f) (Maybe.withDefault 0 (List.head l))

--------------------------------------------------------------------------------------------------------

startList: List Int
startList =
      init 0 255


init: Int -> Int -> List Int
init ix n =
   if n == ix then
      [n]
   else
      ix ::  init(ix + 1) n

findAndMTF: Char -> List Int -> Maybe (Int, List Int)
findAndMTF c l =
   if l == [] then
      Nothing
   else
      let
         ix = find (Char.toCode c) l
         l2 = delete (Char.toCode c) l
      in
         if (ix /= Nothing) then
            Just (Char.toCode c, (Char.toCode c) :: l2)
         else
            Nothing

encodeMTF: String -> Maybe (List Int)
encodeMTF s =
   if s == "" then
      Just []
   else
      let
          split = String.uncons s
          c = Tuple.first (Maybe.withDefault('a', "") split)
          s2 = Tuple.second (Maybe.withDefault('a', "") split)
      in
         if (split == Nothing) then
            Nothing
         else
            Just ((Char.toCode c) :: (Maybe.withDefault [] (encodeMTF s2)))



--predavanje

{-

dist : (Float, Float) -> (Float, Float) -> Float
dist (x1, y1) (x2, y2) = let
                           --x1 = Tuple.first p1
                           --x2 = ...
                           d1 = x2 - x1
                           d2 = y2 - y1
                        in
                           sqrt(d1*d1+d2*d2)

fib : Int -> Maybe Int
fib n =
   if n<1 then
      Nothing
   else if n <= 2 then
      Just 1
   else
      let
          f1 = fib (n - 1)
          f2 = fib (n - 2)
      in
          if f1 /= Nothing && f2 /= Nothing then
             Just (Maybe.withDefault 0 f1 + Maybe.withDefault 0 f2)
          else
             Nothing
-}




main =
   -- Html.text (Debug.toString (foldLeft 0 [2,4] (\x y -> x + y)))
    --Html.text (Debug.toString (foldLeft 0 [2,4] (\x y -> x + y)))
    --Html.text (Debug.toString (foldLeft 0 [2,4] (\x y -> x + y)))
    Html.text (Debug.toString (foldLeft 0 [1,2,3] (\x  y ->  x+y)))
