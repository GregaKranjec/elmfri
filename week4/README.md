# Week 4  Elm@FRI


## Sierpinski triangle

Write a function for drawing the Sierpinski triangle.


In `Main.elm` you are given the following definition:

```elm
mkTriangle : Int -> Int -> Int -> Svg.Svg msg
```
which draws an equilateral triangle. The first two numbers are the coordinates of the bottom left vertex, and the third numbers gives the length of the edge.

With the help of this function you can define
```elm
mkSierpinski : Int -> Int -> Int -> Int -> List (Svg.Svg msg)
```
which receive the coordinates of the bottom left vertex, the length of the edge, and the depth of the recursion for drawing the Sierpinski triangle.

![](primer-sierpinski.png)


## Turtle graphics

Write an interpreter for a very basic subset of the programming language LOGO.

LOGO is a language for moving a small turtle on the screen. During the movement, the turtle can also leave a trace.
For this assignment the instruction set will be reduced to these two commands:
- move forward for the specified number of pixels,
- turn for a specified number of degrees to the right, or to the left if the number is negative.

The turtle is represented as a pair of, the first element of the pair is its x,y position on the plane, the second element is the angle (in degrees) for which the turtle is rotated.
This can be represented in Elm as

```elm
type alias Turtle = ((Int,Int), Int)
```

Your task is to write the following function:

```elm
interpret : Turtle -> List String -> List (Svg.Svg msg)
```
This function accepts two parameters:
- current position of the turtle,
- list of commands yet to be evaluated.
Every instruction is given as a string, which starts with a single letter. After a space, the each instruction has an integer.

The command for moving forward is F, and for rotation is R.
Examples:
- move forward for 100 pixels: `F 100`
- rotate 90 degrees : `R 90`

### Example
For drawing an octagon, we would execute the following LOGO program:
```elm
interpret ( ( 250, 250 ), 90 ) [ "F 100", "R 45", "F 100", "R 45", "F 100", "R 45", "F 100", "R 45", "F 100", "R 45", "F 100", "R 45", "F 100", "R 45", "F 100" ]
```
The obtained result is:

![](primer-logo.png)
