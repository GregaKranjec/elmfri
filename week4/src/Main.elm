module Main exposing (Turtle, drawTurtle, main, mkTriangle)

import Html exposing (..)
import Svg exposing (..)
import Svg.Attributes exposing (..)


--------------------------  Sierpinski -----------------------


mkTriangle : Int -> Int -> Int -> Svg.Svg msg
mkTriangle lx ly side =
    let
        t_height =
            sqrt (toFloat (side * side) * 3.0 / 4.0)

        p1 =
            String.fromInt lx ++ "," ++ String.fromInt ly

        p2 =
            String.fromInt (lx + side) ++ "," ++ String.fromInt ly

        p3 =
            String.fromInt (lx + (side // 2)) ++ "," ++ String.fromInt (ly - (sqrt (toFloat (side * side) * 3.0 / 4.0) |> round))
    in
    Svg.polyline [ points (p1 ++ " " ++ p2 ++ " " ++ p3 ++ " " ++ p1), fill "none", stroke "black" ] []



mkSierpinski : Int -> Int -> Int -> Int -> List (Svg.Svg msg)
mkSierpinski lx ly side depth =
   let
       t_height =
          sqrt (toFloat (side*side) * 3.0/4.0)

       newH = round (toFloat (ly) - t_height/2)
   in
   if depth == 0 then []

   else
      [mkTriangle lx ly side] ++ mkSierpinski lx ly (side//2) (depth-1) ++ mkSierpinski (lx+side//2) ly (side//2) (depth-1) ++ mkSierpinski (lx+side//4) newH (side//2) (depth-1)


--------------------------  Mini LOGO -----------------------


type alias Turtle =
    ( ( Int, Int ), Int )


{-| draw turtle on x, y, angle
-}
drawTurtle : Int -> Int -> Int -> Svg.Svg msg
drawTurtle posx posy angle =
    Svg.image [ xlinkHref "img/turtle.png", height "20", width "20", x (String.fromInt (posx - 10)), y (String.fromInt (posy - 10)), transform ("rotate(" ++ String.fromInt (angle - 90) ++ " " ++ String.fromInt posx ++ " " ++ String.fromInt posy ++ ")") ] []



interpret : Turtle -> List String -> List (Svg.Svg msg)
interpret turtle commands =
   let
       head = List.head commands
       str = String.split " " head
       ukaz = List.head str
       angle = List.tail str
       tail = List.tail commands
       ((x,y), r) = turtle
   in
      if head == [] then []
      else
         line ()

main : Html msg
main =
    --Svg.svg [ width "500px", height "500px", fill "blue" ] [ drawTurtle 250 250 90 ]
    --svg [width "500px", height "500px"] (mkSierpinski 0 500 500 5)
   Svg.svg [ width "500px", height "500px" ] (interpret ( ( 250, 250 ), 90 ) [ "F 100", "R 45", "F 100", "R 45", "F 100", "R 45", "F 100", "R 45", "F 100", "R 45", "F 100", "R 45", "F 100", "R 45", "F 100" ])
