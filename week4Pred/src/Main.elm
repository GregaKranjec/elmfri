module Main exposing (main)

import Html
import Html exposing (..)
import Html.Attributes exposing (..)
--import Svg exposing (..)
--import Svg.Attributes exposing (..)

buttonStyle n = [
               style "height" "50px",
               style "width" "50px",
               style "backgroundColor"
               (if modBy 2 n == 0 then
                  "gray"
                else
                   "white"
                )]

--creates n buttons
listOfButtons : Int -> Int -> List (Html msg)
listOfButtons n row =
   List.map (\x-> button (buttonStyle (x + row)) [text ""]) (List.range 1 n)





main : Html msg
main =
   div [] (List.map (\x -> div [] (listOfButtons 8 x)) (List.range 1 8))
--   svg [height "500px", width "500px"]
--       [rect [ x "50px", y "50px", height "100px", width "100px", fill "blue"] []]

{-
type Student
   = Regular Int String String
   | Exchange Int String String String

name : Student -> String
name s =
   case s of
      Regular _ firstname _ ->
         firstname

      Exchange _ "John" _ _ ->
         "Janez"

      Exchange _ firstname _ _ ->
         firstname

type MyList
   = Empty
   | Cons Int Mylist

length : MyList -> Int
length l =
   case l of
       Empty ->
           0

       Cons _ t ->
           1 + length t

type NEList
   = OneEl Int
   | NECons Int NEList

head : NEList -> Int
head l =
   case l of
       OneEl h ->
          h

       NECons h _ ->
          h

hasMoreThan2 : NEList -> Bool
hasMoreTHan2 l =
   case l of
      NECons _ (NECons _ _) ->
         True

      _ ->
         False

type Nat
   = One
   | S Nat

plus : Nat -> Nat -> Nat
plus n m =
   case n of
       One ->
           S m

       S n_p ->
          S (plus n_p m)

fib : Nat -> Int
fib n =
   case n of
       One ->
          1

       S One ->
          1

       S (S m) ->
          fib m + fib (S m)




main =
   Html.text "hey pattern"
-}
