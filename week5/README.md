# Week 5 Elm@FRI


## Assignment 1
Basic functionality of binary search trees (BSTs)

```elm
type BST
    = Empty
    | BST Int BST BST
```
Implement the functions:

```elm
add : Int -> BST -> BST
depth : BST -> Int
makeTree : List Int -> BST
dfs : BST -> List Int
bfs : BST -> List Int
```

1. The first function adds a number to the binary search tree (it returns a new tree with that number added, it does not change it).
2. The second function returns the depth (height) of the BST.
3. The function 'makeTree' builds a BST from the given list of elements.
4. The function 'dfs'  returns a list of numbers from the BST as they were searched in the depth-first order (dfs).
5. The function 'bfs'  returns a list of numbers from the BST as they were searched in the breadth-first order (bfs).


## Assignment 2 (drawing binary trees).
There are a lot of strategies for drawing binary trees. You have to implement a strategy in the following manner:
Imagine you have a complete tree (e.g. there are nodes present up the largest depth). We can assign two coordinates (x and y) to every node, using the rules:
```
x = sequential number of the vertex if the tree is traversed in-order (remember - in a complete tree!).
y = the depth at which the vertex is located.
```

First, define the function :

 ```elm
 getCoords : BST -> List (Int,Int,Int)
 ```
that gets a BST and returns a list of triplets (x, y, el) - e.g. a list of coordinates (x, y), at which el is located.

 Then implement the function

 ```elm
 draw : List ( Int, Int, Int ) -> List (Svg.Svg msg)
 ```
which accepts the computed list of coordinates and draws the tree in SVG (you do not have to draw the connections between nodes

A very simple example of the visualization can be seen below.

```elm
BST 5 (BST 3 (BST 2 Empty Empty) (BST 4 Empty Empty)) (BST 6 (BST 5 Empty Empty) (BST 7 (BST 6 Empty Empty) Empty))
```

This is the SVG representation of the above tree:

![alt text](primer_drevesa.png)
