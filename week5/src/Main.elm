module Main exposing (BST(..), main, add, singleton, empty, depth, makeTree, dfs, expandList)

import Html
import Svg exposing (..)
import Svg.Attributes exposing (color, cx, cy, fill, height, r, stroke, width, x, y)


type BST
    = Empty
    | BST Int BST BST


----------BST-------------------------

empty : BST
empty =
   Empty

singleton : Int -> BST
singleton i =
   BST i Empty Empty

add : Int -> BST -> BST
add n tree =
   case tree of
       Empty ->
           singleton n

       BST m left right ->
          if n > m then
             BST m left (add n right)
          else if  n < m then
             BST m (add n left) right
          else tree



depth : BST -> Int
depth tree =
   case tree of
       Empty ->
          0

       BST _ left right ->
          1 + max (depth left) (depth right)

makeTree : List Int -> BST
makeTree list =update : Msg -> Expr -> Expr
update msg expresion =
  case msg of
    Change ->
   case list of
      [] ->
         empty
      h :: t ->
         add h (makeTree t)

dfs : BST -> List Int
dfs tree =
   case tree of
       Empty ->
           []

       BST x left right ->
          [x] ++ dfs left ++  dfs right

expandList : List BST -> List BST
expandList list =
   List.concatMap expand list

expand : BST -> List BST
expand tree =
   case tree of
       Empty ->
           []

       BST _ left right ->
          [left, right]


{-

fold_left : List BST -> Int

fold_left list =
   case list of
      [] ->
         0
      h :: t ->

-}



bfs : BST -> List Int
bfs tree =
   case tree of
        Empty ->
            []

        BST x _ _ ->
          [x] ++




----------visualization-------------------------
-- draw : List ( Int, Int, Int ) -> List (Svg.Svg msg)
--getCoords : Int -> Int -> BST -> List ( Int, Int, Int )


main =
    Html.pre [] [ Html.text ("123") ]
