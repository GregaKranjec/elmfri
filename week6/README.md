# Week 6 Elm@FRI

This week, your applications will become dynamic.

## Asssignment 1
Implement a line of lights that can be turned on and off with a mouse click.

![](ex1.png)

The data model for this application is simply a list of boolean values, which represent which lights are turned on, and which are turned off.

```elm
type alias LightsLine = List Bool

```
In order for you to start the assignement faster, there is already an auxiliary function implemented for you in the file Main.elm

```elm
toggleElement : Int -> LightsLine -> LightsLine
```
which accepts an indeks and a list, and toggles the boolean value at the given index.

The messages the the Html elements can send are of the form:

```elm
type Msg = Toggle Int
```
Every ligh should send its index when clicked on it.

In order to connect all components into a dynamic application, implement the following two methods:

```elm
view : LightsLine -> Html Msg
update : Msg -> LightsLine -> LightsLine
```
and use them in the main function.

## Assignment 2.

The second assignment is an upgrade of the first one. Your task is to implement a game, called Lights Out. The rules of the game can be found in:
[ LightsOut ](https://en.wikipedia.org/wiki/Lights_Out_(game)).


The game board is a two-dimensional table of lights, which can be modeled with the following data type:

```elm
type alias LightsTable = List LightLine

```

In Main.elm there is already a useful method for toggling an arbitrary light:

```elm
tableToggle : Int -> Int -> LightsTable -> LightsTable
```

The message data type is augmented by a second coordinate:

```elm
type Msg = Toggle Int Int
```
When this message is triggered you have to toggle the light at this coordinate, but also toggle all of its neighbours.


An example behavior is shown in the following animated gif.

![](play.gif)
