module Main exposing (..)
import Browser exposing (sandbox)
import Html exposing(..)
import Html.Events exposing (onClick)
import Html.Attributes exposing(..)

{-}

type alias LightsLine = List Bool
type Msg = Toggle Int

toggleElement : Int -> List Bool -> List Bool
toggleElement idx l =
    case ( l, idx ) of
        ( [], _ ) ->
            l

        ( e :: t, 0 ) ->
            not e :: t

        ( e :: t, i ) ->
            e :: toggleElement (idx - 1) t

view : LightsLine -> Html Msg
view l=
     let
        newL = List.indexedMap Tuple.pair l
     in
     div[]
           (List.map(\x-> if Tuple.second(x) == True then button[onClick (Toggle (Tuple.first(x))) ][img[src "bulb-on.png"][]] else button[onClick (Toggle (Tuple.first(x)))] [img[src "bulb-off.png"][]] ) newL)

init: Int -> LightsLine
init i =
       List.map (\x -> if modBy 2 x ==0 then True else False) (List.range 0 i)
update : Msg -> LightsLine -> LightsLine
update msg l =
       let
         newL = List.indexedMap Tuple.pair l
       in
       case msg of
            Toggle i -> List.map(\x -> if Tuple.first(x) == i then not (Tuple.second(x)) else Tuple.second(x)) newL

main =
     Browser.sandbox{init= (init 6) ,update = update,view = view}


-}
-- SECOND EXERCISE

type alias LightsLine = List Bool
type alias LightsTable =
    List LightsLine

type Msg = Toggle Int Int

toggleElement : Int -> List Bool -> List Bool
toggleElement idx l =
    case ( l, idx ) of
        ( [], _ ) ->
            l

        ( e :: t, 0 ) ->
            not e :: t

        ( e :: t, i ) ->
            e :: toggleElement (idx - 1) t


tableToggle : Int -> Int -> LightsTable -> LightsTable
tableToggle idx idy table =
    case ( table, idx ) of
        ( [], _ ) ->
            table

        ( e :: t, 0 ) ->
            toggleElement idy e :: t

        ( e :: t, i ) ->
            e :: tableToggle (idx - 1) idy t

view : LightsTable -> Html Msg
view lt =
       let
          prvaLista=List.indexedMap Tuple.pair lt
       in
       div[](List.map(\(l,y) -> drawLights l y ) prvaLista)
drawLights :  Int ->LightsLine -> Html Msg
drawLights y l =
           let
              newL = List.indexedMap Tuple.pair l
           in
           div[](List.map(\x-> if Tuple.second(x) == True then button[onClick (Toggle (Tuple.first(x)) y) ][img[src "bulb-on.png"][]] else button[onClick (Toggle (Tuple.first(x)) y)] [img[src "bulb-off.png"][]] ) newL)
init : Int -> Int -> LightsTable
init i j =
        let
           prvaLista=List.map(\x ->if modBy 2 x ==0 then True else False) (List.range 0 i)
           drugaLista=List.map(\x ->if x==True ||x==False then  List.map(\y->if modBy 2 y ==0 then True else False)(List.range 0 j)else []) prvaLista
        in
        drugaLista
update: Msg -> LightsTable -> LightsTable
update msg lt=
           case msg of
                Toggle i j -> tableToggle j i lt |> tableToggle (j+1)i |> tableToggle (j-1) i|> tableToggle j (i+1) |> tableToggle j (i-1)
main=
     Browser.sandbox { init = (init 5 5), update = update, view = view }
