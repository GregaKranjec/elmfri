# Week 7 Elm@FRI


## Assignment 1
Upgrade the assignment from last week (LightOut) so that at the initialization, the lights will be turned on randomly.

## Assignment 2.
Implement a simple quiz application that asks questions to the user and checks her answers.

The basic model of the application is summarized in the following elm code.

```elm
type alias Question =
    String


type alias Answer =
    String


type alias Model =
    { question : Question
    , answer : Answer
    , currAnswer : Answer
    , checked : Bool
    , correct : Bool
    , countCorrect : Int
    }
```

At a certain point in the application one has:
1. current question (question),
2. the correct answer to the current question (answer),
3. currently active answer (what is written in the text field) (currAnswer),
4. was this answer already checked (checked),
5. is the current answer correct,
6. correct answers count (countCorrect).

The collection of question and answers is already given in the module Questions as a list of pairs (Question, Answer).
All questions have a single word as the answer, and they are considered correct if they are completely equal.
Your application should choose th questions randomly.

A simple example of the game is given below:

![](example.gif)
