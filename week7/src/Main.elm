module Main exposing (Answer, Model, Msg(..), Question, model, getQuestion, getLength)

import Browser
import Html exposing (..)
import Html.Events exposing (onClick, onInput)
import Questions
import Random


-- Predavanje --
{-}
type alias Employee =
  { name : String,
    surname = String,
    id = Int
  }

getName : Employee -> String
getName e =
  e.name


changeSurname : Employee -> String -> Employee
changeSurname e newSurname =
  {e | surname = newSurname}

-- Employee "John" "Doe" 42

randomInt : Int -> Int
randomInt seed =
  modBy (2 ^ 32)(1103515426 * seed + 12345)

randomSeq: Int -> Int -> List Int
randomSeq s length =
  if length == 0 then
    []
  else
    (randomInt s) :: (randomSeq (randomInt s) (length-1))

-- Random with Elm architecture --

type alias Model =
  { diceValue : Int
  }

type Msg
  = Roll
  | NewDice Int

view : Model -> Html Msg
view m =
  Html.div [] [ Html.text (String.fromInt m.NewDice diceValue), Html.button [ onClick Roll] [Html.text "Throw"]]

update : Msg -> Mode -> (Model, Cmd.Cmd Msg)
update msg m =
  case msg of
      Roll -> (m, Random.generate NewDice(Random.int 1 6))
      NewDice n -> ({m|diceValue = n}, Cmd.none)

init : () -> (Model, Cmd Msg)
init _ =
  ({diceValue = 5}, Cmd.none)

-}

type alias Question =
    String


type alias Answer =
    String


type alias Model =
    { question : Question
    , answer : Answer
    , currAnswer : Answer
    , checked : Bool
    , correct : Bool
    , countCorrect : Int
    }


type Msg
    = Check
    | CurrAnsw String
    | NewQ
    | RandQA Int


model : Model
model =
    { question = ""
    , answer = ""
    , currAnswer = ""
    , checked = False
    , correct = False
    , countCorrect = 0
    }

getQuestion : Model -> String
getQuestion m =
  m.question

getLength : Int
getLength =
  List.length (Questions.trivia)


view : Model -> Html Msg
view m =
  Html.div [] [ Html.text (getQuestion model), Html.button [ onClick RandomQA] [Html.text "Answer"]]



update : Msg -> Mode -> (Model, Cmd.Cmd Msg)
update msg m =
  case msg of
      Check -> m.correct
      CurAnsw s ->
      NewQ -> getRandomQuestion(0, getLength)
      RandomQA n -> ({m | }, Cmd.none)

{-
Roll -> (m, Random.generate NewDice(Random.int 1 6))
NewDice n -> ({m|diceValue = n}, Cmd.none)
-}


init : () -> (Model, Cmd Msg)
init _ =
  ({}, )







-- view : Model -> Html Msg
main = Html.div [] [Html.text "what"]
  --  Browser.element { init = init, view = view, update = update, subscriptions = \_ -> Sub.none }
