
-- DATA --

{-

type Expr
    = Val Int
    | Var String
    | Plus Expr Expr
    | Times Expr Expr

-------------------- Naloga 1 --------------

parse : String -> Maybe Expr
parse s =
  let
    list = String.split " " s
    finalStack = List.foldl (\v stack ->
                   case (v,stack) of
                     ("*", e1::e2::rest) ->
                       (Times e1 e2) :: rest

                     ("+", e1::e2::rest) ->
                       (Plus e1 e2) :: rest

                     (_,_) ->
                       case String.toInt v of
                         Nothing ->
                           (Var v) :: stack

                         Just n ->
                           (Val n) :: stack

                 ) [] list
  in
    case finalStack of
      [e] -> Just e
      _ -> Nothing


show : Expr -> String
show expresion =
  case expresion of
      Val n ->
        String.fromInt n

      Var n ->
        n

      Plus e1 e2 ->
        show e1 ++ "+" ++ show e2

      Times e1 e2 ->
        show e1 ++ "*" ++ show e2




------------------- Naloga 2 -----------------

apiurl = "https://newton.now.sh/simplify/"

type alias Model =
    { expr : Maybe Expr
    , simplified : String
    }


type Msg
    = Parse String
    | Simplified (Result Http.Error String)



init: () -> (Model, Cmd Msg)
init _ =
  (Model Nothing "", Cmd.none)

view : Model -> Html.Html Msg
view model =
  Html.div [] [Html.input[onInput Parse][], Html.br[][], Html.text("Current Expr"), Html.br[][],
  Html.text(show(Maybe.withDefault (Val 0) model.expr)), Html.br [][], Html.text("Simplified Expr"), Html.br[][],
  Html.text(model.simplified), Html.br[][]]

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
    Parse str -> ({model | expr = parse str}, Http.get {expect = Http.expectJson Simplified (D.field "result" D.string), url = apiurl++show(Maybe.withDefault(Val 0) (model.expr))})
    _ -> (model, Cmd.none)





main =
   Browser.element
       { init = init
       , view = view
       , update = update
       , subscriptions = \_ -> Sub.none
       }

{-
main =
  Debug.toString(show (Maybe.withDefault (Val 0) (parse "x 5 * y +")) )|> Html.text
-}
-}
