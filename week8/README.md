# Week 8 Elm@FRI



## Assignment 1

Simple arithmetic expressions can be represented with the following data type in elm:

```elm
type Expr
    = Val Int
    | Var String
    | Plus Expr Expr
    | Times Expr Expr
```
E.g. the expression `x*y+5` can be represented as `Plus (Times (Var x) (Var y) ) (Val 5)`.

First write a function that builds an expression from a string.
The expression in the string is given in the postfix notation, e.g. "x y + 5 *" represents the expression `Plus (Times (Var x) (Var y) ) (Val 5)`.

```elm
parse : String -> Maybe Expr
```

The second function, given an `Expr`, constructs a string representation of this expression in a more common notation, i.e. the infix notation.
The expression given earlier would be represented as: `x*y+5`.

```elm
show : Expr -> String
```


## Assignment 2.

For the second assignment you will have to use a remote API. The remote API will be used for simplification of expressions.

The user of your application enters an expression in postfix form. This expression is then sent to a remote server for simplification and, when returned, is displayed back to the user.

You will use two types of messages:
```elm
type Msg
    = Parse String
    | Simplified (Result Http.Error String)
```
The first one is used by the input field to send the user input. The second message will be used by the Elm system to return back the result of the remote API call.

The remote API, which is capable of simplifying arithmetic expressions is accessible at: https://newton.now.sh/simplify/

Play with the API first using the browser, so you will see what is the data returned back.

An example of the application can be seen on the GIF below:
![](example.gif)
