module Main exposing (main)

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Http
import Json.Decode as D

difficulty = 5


apiurl =
    "https://api.wordnik.com/v4/words.json/randomWord?hasDictionaryDef=true&includePartOfSpeech=noun&minCorpusCount=15000&maxCorpusCount=-1&minDictionaryCount=1&maxDictionaryCount=-1&minLength=3&maxLength=6&api_key=a4d8be8a717ebd9730005081d1102ea8717bcb434ca2b85d9"

--apiurl2 = "https://api.wordnik.com/v4/words.json/randomWord?hasDictionaryDef=true&includePartOfSpeech=noun&" ++ difficulty ++ "&maxCorpusCount=-1&minDictionaryCount=1&maxDictionaryCount=-1&minLength=3&maxLength=10&api_key=a4d8be8a717ebd9730005081d1102ea8717bcb434ca2b85d9"

type alias RevealedWord =
  { pos : Int, text : String }

type alias Model =
    { word : String,
      guess : String,
      revealedWord : RevealedWord,
      result : GuessResult,
      score : Int
    }

type alias GuessResult =
  { text : String, isCorrect : Bool }

type Msg
    = Kick
    | Answer String
    | Check
    | Reveal
    | NewWord (Result Http.Error String)



initalWordList : List String
initalWordList =
    ["Space", "Book", "Bottle", "Ball", "lock", "lamp", "street", "pavement",
    "computer", "fire", "speed", "word", "youth", "attack", "scream", "foul",
    "airplane", "rock", "system", "steal", "bag", "ring", "tower", "king"]

init : () -> ( Model, Cmd Msg )
init _ =
    ( Model "Click to get a definition" "" {pos = 2, text = ""} { text = "", isCorrect = False } 0, Cmd.none )


view : Model -> Html Msg
view m =
    Html.div [
      style "textAlign" "center",
      style "fontSize" "30px",
      style "fontFamily" "arial",
      style "backgroundColor" "#f9f8f7",
      style "height" "71vh",
      style "padding-top" "27vh",
      style "overflow" "hidden"
    ] [ Html.div [style "fontSize" "64px", style "color" "orange", style "fontFamily" "Helvetica"] [Html.text "WordGuess"],
        Html.br [] [] ,
        Html.text ("The word stars with " ++ m.revealedWord.text ++ " and has "),
        Html.text (String.fromInt (String.length m.word) ++ " letters"),
        Html.br [] [],
        Html.br [] [],
        Html.button [ onClick Kick,
                    style "color" "white",
                    style "width" "6vw",
                    style "height" "5vh",
                    style "fontFamily" "arial",
                    style "fontSize" "16px",
                    style "backgroundColor" "#247b9e",
                    style "border" "none",
                    style "borderRadius" "5px",
                    style "text-transform" "uppercase"] [ Html.text "New word" ],
        Html.br [] [],
        Html.br [] [],
        Html.input [ onInput Answer,
                    style "textAlign" "center",
                    style "width" "20vw",
                    style "height" "3vh",
                    style "color" "white",
                    style "backgroundColor" "#11203a",
                    style "border" "none",
                    style "border-radius" "5px",
                    placeholder "Guess"] [],
        Html.br [] [],
        Html.br [] [],
        Html.button [ onClick Check,
                    style "color" "white",
                    style "width" "6vw",
                    style "height" "5vh",
                    style "fontFamily" "arial",
                    style "fontSize" "16px",
                    style "backgroundColor" "#d68126  ",
                    style "border" "none",
                    style "borderRadius" "5px",
                    style "text-transform" "uppercase",
                    style "margin-right" "2vw" ] [ Html.text "Submit" ],
        Html.button [ onClick Reveal,
                    style "color" "white",
                    style "width" "6vw",
                    style "height" "5vh",
                    style "fontFamily" "arial",
                    style "fontSize" "16px",
                    style "backgroundColor" "#247b9e",
                    style "border" "none",
                    style "borderRadius" "5px",
                    style "text-transform" "uppercase" ] [ Html.text "Reveal"],
        Html.br [] [],
        Html.br [] [],
      {-}  Html.button [ onClick Reveal,
                    style "backgroundColor" "#4cba2e",
                    style "border" "none",
                    style "width" "5vw",
                    style "height" "3vh",
                    style "borderRadius" "5px",
                    style "margin-right" "1vw"] [ Html.text "Easy"],
        Html.button [ onClick Reveal,
                    style "backgroundColor" "#e28624",
                    style "border" "none",
                    style "width" "5vw",
                    style "height" "3vh",
                    style "borderRadius" "5px",
                    style "margin-right" "1vw"] [ Html.text "Medium"],
        Html.button [ onClick Reveal,
                    style "backgroundColor" "#ba2e33",
                    style "border" "none",
                    style "width" "5vw",
                    style "height" "3vh",
                    style "borderRadius" "5px",
                    style "margin-right" "1vw" ] [ Html.text "Hard"], -}
        genResult m
      ]


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Kick ->
            ( model, Http.get { expect = Http.expectJson NewWord (D.field "word" D.string), url = apiurl } )

        Answer text ->
            ({ model | guess = text}, Cmd.none)

        Check ->
            ({model | result = checkResult model}, Cmd.none)

        Reveal  ->
            ({ model | revealedWord = revealAndIncrement model }, Cmd.none)

        NewWord (Ok word) ->
            ({ model | word = word, guess = "", revealedWord = {pos = 2, text = String.slice 0 1 word}, result = { text = "", isCorrect = False }}, Cmd.none)

        NewWord (Err _) ->
            ({ model | word = "ERROR"},Cmd.none)



checkResult : Model -> GuessResult
checkResult { word, guess, result, score } =
  if String.toLower word == String.toLower guess then
    { result | text = "You got it!", isCorrect = True }
  else
    { result | text = "Nope", isCorrect = False }


genResult : Model -> Html Msg
genResult { result } =
    if String.isEmpty result.text then
        div [] []
    else
        let
            color =
                if result.isCorrect then
                    "forestgreen"
                else
                    "tomato"
        in
            Html.div
                [ style "color" color,
                  style "fontFamily" "arial"
                ]
                [text result.text]

revealAndIncrement : Model -> RevealedWord
revealAndIncrement {revealedWord, word} =
  if revealedWord.text == word then
    revealedWord
  else
    { revealedWord | pos = revealedWord.pos +1, text = String.slice 0 revealedWord.pos word }

main =
    Browser.element { init = init, update = update, view = view, subscriptions = \_ -> Sub.none }
